class Box {
  public double width;
  public double height;
  public double depth;

  Box(double w, double h, double d) {
    width = w;
    height = h;
    depth = d;
  }

  public double volume() {
    return width * height * depth;
  }
}

class Main {
  public static void main(String args[]) {
    Box box = new Box(5, 10, 20);

    System.out.println("Volume is " + box.volume());
  }
}
